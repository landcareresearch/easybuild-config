easybuild-config
================

Environment variables setting for EasyBuild

# Description

easybuild.sh: Configuration file for EasyBuild to be placed in /etc/profile.d

libc6.preseed: Preseed file for automatic installation of libc6 on Debian.
