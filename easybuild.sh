# initialisation des variables d'environnement d'EasyBuild (a placer dans /etc/profile.d)
# Ajout de module et de ses variables d'environnement au bash
. /usr/share/lmod/lmod/init/profile

export EASYBUILD_SOURCEPATH=/opt/apps/EasyBuild/sources
export EASYBUILD_BUILDPATH=/opt/apps/EasyBuild/build
export EASYBUILD_INSTALLPATH=/opt/apps/EasyBuild
export MODULEPATH=$EASYBUILD_INSTALLPATH/modules/bio:$EASYBUILD_INSTALLPATH/modules/cae:$EASYBUILD_INSTALLPATH/modules/chem:$EASYBUILD_INSTALLPATH/modules/compiler:$EASYBUILD_INSTALLPATH/modules/data:$EASYBUILD_INSTALLPATH/modules/debugger:$EASYBUILD_INSTALLPATH/modules/devel:$EASYBUILD_INSTALLPATH/modules/geo:$EASYBUILD_INSTALLPATH/modules/lang:$EASYBUILD_INSTALLPATH/modules/lib:$EASYBUILD_INSTALLPATH/modules/math:$EASYBUILD_INSTALLPATH/modules/mpi:$EASYBUILD_INSTALLPATH/modules/numlib:$EASYBUILD_INSTALLPATH/modules/perf:$EASYBUILD_INSTALLPATH/modules/phys:$EASYBUILD_INSTALLPATH/modules/system:$EASYBUILD_INSTALLPATH/modules/toolchain:$EASYBUILD_INSTALLPATH/modules/tools:$EASYBUILD_INSTALLPATH/modules/vis:$EASYBUILD_INSTALLPATH/modules/base:$MODULEPATH
export EASYBUILD_REPOSITORY=FileRepository
export EASYBUILD_REPOSITORYPATH=$EASYBUILD_INSTALLPATH/ebfiles_repo
export EASYBUILD_LOGFILE_FORMAT=("easybuild", "easybuild-%(name)s-%(version)s-%(date)s.%(time)s.log")
export EASYBUILD_MODULES_TOOL=Lmod
export EASYBUILD_MODULE_NAMING_SCHEME=EasyBuildMNS
